From 46a2ce35212c9b35c4818ca9eec918aa4a45cb48 Mon Sep 17 00:00:00 2001
From: Jan Beulich <jbeulich@suse.com>
Date: Tue, 24 Sep 2024 14:28:22 +0200
Subject: [PATCH 16/83] Arm: correct FIXADDR_TOP

While reviewing a RISC-V patch cloning the Arm code, I noticed an
off-by-1 here: FIX_PMAP_{BEGIN,END} being an inclusive range and
FIX_LAST being the same as FIX_PMAP_END, FIXADDR_TOP cannot derive from
FIX_LAST alone, or else the BUG_ON() in virt_to_fix() would trigger if
FIX_PMAP_END ended up being used.

While touching this area also add a check for fixmap and boot FDT area
to not only not overlap, but to have at least one (unmapped) page in
between.

Fixes: 4f17357b52f6 ("xen/arm: add Persistent Map (PMAP) infrastructure")
Signed-off-by: Jan Beulich <jbeulich@suse.com>
Reviewed-by: Michal Orzel <michal.orzel@amd.com>
master commit: fe3412ab83cc53c2bf2c497be3794bc09751efa5
master date: 2024-08-13 21:50:55 +0100
---
 xen/arch/arm/include/asm/fixmap.h | 2 +-
 xen/arch/arm/mmu/setup.c          | 6 ++++++
 2 files changed, 7 insertions(+), 1 deletion(-)

diff --git a/xen/arch/arm/include/asm/fixmap.h b/xen/arch/arm/include/asm/fixmap.h
index a823456ecb..0cb5d54d1c 100644
--- a/xen/arch/arm/include/asm/fixmap.h
+++ b/xen/arch/arm/include/asm/fixmap.h
@@ -18,7 +18,7 @@
 #define FIX_LAST FIX_PMAP_END
 
 #define FIXADDR_START FIXMAP_ADDR(0)
-#define FIXADDR_TOP FIXMAP_ADDR(FIX_LAST)
+#define FIXADDR_TOP FIXMAP_ADDR(FIX_LAST + 1)
 
 #ifndef __ASSEMBLY__
 
diff --git a/xen/arch/arm/mmu/setup.c b/xen/arch/arm/mmu/setup.c
index f4bb424c3c..57042ed57b 100644
--- a/xen/arch/arm/mmu/setup.c
+++ b/xen/arch/arm/mmu/setup.c
@@ -128,6 +128,12 @@ static void __init __maybe_unused build_assertions(void)
 
 #undef CHECK_SAME_SLOT
 #undef CHECK_DIFFERENT_SLOT
+
+    /*
+     * Fixmaps must not overlap with boot FDT mapping area. Make sure there's
+     * at least one guard page in between.
+     */
+    BUILD_BUG_ON(FIXADDR_TOP >= BOOT_FDT_VIRT_START);
 }
 
 lpae_t __init pte_of_xenaddr(vaddr_t va)
-- 
2.47.0

